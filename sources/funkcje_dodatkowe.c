#include "funkcje_dodatkowe.h"


void pokaz_blad(char *tekst, GtkWidget *window)
{
    GtkWidget *dialog;
    dialog = gtk_message_dialog_new(GTK_WINDOW(window),
                                    GTK_DIALOG_DESTROY_WITH_PARENT,
                                    GTK_MESSAGE_ERROR,
                                    GTK_BUTTONS_OK,
                                    tekst);
    gtk_window_set_title(GTK_WINDOW(dialog), "Błąd");
    GtkWidget *obrazek=gtk_image_new_from_file("dane/logo.png");
    gtk_window_set_icon(GTK_WINDOW(dialog), gtk_image_get_pixbuf(GTK_IMAGE(obrazek)));

    gtk_dialog_run (GTK_DIALOG (dialog));
    gtk_widget_destroy (dialog);
}

void pokaz_info(char *tekst, GtkWidget * window)
{
    GtkWidget *dialog;
    dialog = gtk_message_dialog_new(GTK_WINDOW(window),
                                    GTK_DIALOG_DESTROY_WITH_PARENT,
                                    GTK_MESSAGE_INFO,
                                    GTK_BUTTONS_OK,
                                    tekst);
    GtkWidget *obrazek=gtk_image_new_from_file("dane/logo.png");
    gtk_window_set_icon(GTK_WINDOW(dialog), gtk_image_get_pixbuf(GTK_IMAGE(obrazek)));
    gtk_dialog_run(GTK_DIALOG(dialog));
    gtk_widget_destroy(dialog);
}

ranking *utworz()
{
    ranking *nowy=malloc(sizeof(ranking));
    for(int i=0; i<10; i++)
    {
        strcpy(nowy->imie[i], "-\n");
        nowy->wynik[i]=0;
    }
    nowy->suma_kontrolna=policz_sume_kontrolna(nowy);
    return nowy;
}

int policz_sume_kontrolna(ranking *r)
{
    int wynik=0;
    for(int i=0; i<10; i++)
    {
        wynik+=strlen(r->imie[i]);
        wynik+=r->wynik[i];
    }
    return wynik;
}

void dopisz_do_rankingu(char *imie, int wynik, ranking *r)
{
    for(int i=0; i<10; i++)
    {
        if(r->wynik[i]<wynik)
        {
            for(int j=8; j>i; j--)
            {
                r->wynik[j]=r->wynik[j-1];
                strcpy(r->imie[j], r->imie[j-1]);
            }
            strcpy(r->imie[i], imie);
            r->wynik[i]=wynik;
            break;
        }
    }
}

ranking *wczytaj(int wysokosc, int szerokosc, int czy_multi)
{
    FILE *plik;
    char nazwa[100];
    char wejscie[100];
    char imie[100];
    int wynik;
    sprintf(nazwa, "dane/rankingi/%d_%d_%d.txt", wysokosc, szerokosc, czy_multi);
    plik=fopen(nazwa, "r");

    ranking *nowy=utworz();
    if(plik==NULL)
        return nowy;
    int i=0;
    if(fgets (wejscie , 100 , plik)!=NULL)  //ma sume kontrolna
    {
        nowy->suma_kontrolna=atoi(wejscie);
        while(fgets (imie , 100 , plik) != NULL  && i<10)
        {
            if ( fgets (wejscie , 100 , plik) != NULL )
            {
                wynik=atoi(wejscie);
                dopisz_do_rankingu(imie, wynik, nowy);
                i++;
            }

        }
    }
    fclose(plik);
    return nowy;
}

void zapisz(int wysokosc, int szerokosc, int czy_multi, ranking*r)
{
    FILE *plik;
    char nazwa[100];
    sprintf(nazwa, "dane/rankingi/%d_%d_%d.txt", wysokosc, szerokosc, czy_multi);
    plik=fopen(nazwa, "w");

    printf("\n\nZAPISYWANIE\n");
    sprintf(nazwa, "%d\n", policz_sume_kontrolna(r));
    fputs(nazwa, plik);

    for(int i=0; i<10; i++)
    {
        fputs(r->imie[i], plik);
        sprintf(nazwa, "%d\n", r->wynik[i]);
        fputs(nazwa, plik);
    }
    free(r);
    fclose(plik);
}

void wyswietl_ranking(GtkWidget *widget,gpointer data)
{
    if(czy_ranking_wlaczony==true)
    {
        return;
    }
    czy_ranking_wlaczony=true;
    okno_ranking *okno=malloc(sizeof(okno_ranking));
    okno->window=gtk_window_new(GTK_WINDOW_TOPLEVEL);
    gtk_window_set_title(GTK_WINDOW(okno->window),"Ranking");
    gtk_window_set_position(GTK_WINDOW(okno->window),GTK_WIN_POS_CENTER);
    gtk_container_set_border_width(GTK_CONTAINER(okno->window), 5);

    gtk_window_set_resizable(GTK_WINDOW(okno->window), FALSE);

    okno->box=gtk_box_new(GTK_ORIENTATION_VERTICAL, 0);
    gtk_container_add(GTK_CONTAINER(okno->window), okno->box);

    okno->combo_box_rozmiar=gtk_combo_box_text_new();
    wczytaj_rozmiary_ranking(okno);
    rezerwuj_pamiec_ranking(okno);
    utworz_elementy_ranking(okno);

    g_signal_connect(G_OBJECT(okno->radio1), "clicked", G_CALLBACK(radio_1), (gpointer) okno);
    g_signal_connect(G_OBJECT(okno->radio2), "clicked", G_CALLBACK(radio_2), (gpointer) okno);
    g_signal_connect(G_OBJECT(okno->combo_box_rozmiar),"changed",G_CALLBACK(zmiana_rankingu),(gpointer) okno);
    g_signal_connect(G_OBJECT(okno->window),"destroy",G_CALLBACK(zwolnij_pamiec_ranking),(gpointer) okno);


    gtk_combo_box_set_active(GTK_COMBO_BOX(okno->combo_box_rozmiar), 0);
    gtk_widget_show_all(okno->window);
}

void wczytaj_rozmiary_ranking(okno_ranking *okno)
{
    printf("WCZYTUJE ROZMIARY\n");
    FILE *plik;
    plik=fopen("dane/rozmiary.txt", "r");
    if(plik==NULL)
        exit(666);
    fscanf(plik, "%d", &okno->ile_rozmiarow);
    printf("%d\n", okno->ile_rozmiarow);
    okno->rozmiary_planszy=malloc(sizeof(int*)*okno->ile_rozmiarow);

    for(int i=0; i<okno->ile_rozmiarow; i++)
    {
        okno->rozmiary_planszy[i]=malloc(sizeof(int)*2);
        fscanf(plik, "%d %d", &okno->rozmiary_planszy[i][0], &okno->rozmiary_planszy[i][1]);
        printf("%d %d\n", okno->rozmiary_planszy[i][0], okno->rozmiary_planszy[i][1]);
    }
    fclose(plik);
}

void zwolnij_pamiec_ranking(GtkWidget *widget,gpointer data)
{
    okno_ranking *temp=(okno_ranking*)data;
    for(int i=0; i<temp->ile_rozmiarow; i++)
    {
        free(temp->rozmiary_planszy[i]);
    }

    free(temp->rozmiary_planszy);
    free(temp->numer);
    free(temp->punkty);
    free(temp->imie);

    free(temp);
    czy_ranking_wlaczony=false;
    printf("ZWOLNIONO PAMIEC Z RANKINGU\n");
}


void rezerwuj_pamiec_ranking(okno_ranking *okno)
{
    char text[50];
    for(int i=0; i<okno->ile_rozmiarow; i++)
    {

        sprintf(text, "%dx%d", okno->rozmiary_planszy[i][0], okno->rozmiary_planszy[i][1]);
        gtk_combo_box_text_insert_text(GTK_COMBO_BOX_TEXT(okno->combo_box_rozmiar), i, text);
    }

    okno->numer=malloc(sizeof(GtkWidget*)*10);
    okno->punkty=malloc(sizeof(GtkWidget*)*10);
    okno->imie=malloc(sizeof(GtkWidget*)*10);
    for(int i=0; i<10; i++)
    {
        sprintf(text, "%d", i+1);
        okno->numer[i]=gtk_label_new(text);
        okno->punkty[i]=gtk_label_new("0");
        okno->imie[i]=gtk_label_new("-");
    }

}

void zmiana_rankingu(GtkWidget *widget,gpointer data)
{
    char tekst[100];
    okno_ranking *temp=(okno_ranking*)data;
    int pomocnik;
    pomocnik=gtk_combo_box_get_active(GTK_COMBO_BOX(temp->combo_box_rozmiar));

    int wysokosc, szerokosc;
    wysokosc=temp->rozmiary_planszy[pomocnik][0];
    szerokosc=temp->rozmiary_planszy[pomocnik][1];


    ranking *r=wczytaj(wysokosc, szerokosc, temp->czy_multi);
    for(int i=0; i<10; i++)
    {
        gtk_label_set_text(GTK_LABEL(temp->imie[i]), r->imie[i]);
        sprintf(tekst, "%d", r->wynik[i]);
        gtk_label_set_text(GTK_LABEL(temp->punkty[i]), tekst);
    }

    free(r);
}

void utworz_elementy_ranking(okno_ranking *okno)
{
    okno->obrazek=gtk_image_new_from_file("dane/logo.png");
    gtk_window_set_icon(GTK_WINDOW(okno->window), gtk_image_get_pixbuf(GTK_IMAGE(okno->obrazek)));
    okno->grid=gtk_grid_new();
    gtk_box_pack_start(GTK_BOX(okno->box), okno->grid, TRUE, TRUE, 0);
    const char *format16 ="<span font_desc=\"16.0\" >%s</span>";
    const char *format12 ="<span font_desc=\"12.0\" >%s</span>";
    char *markup;
    markup = g_markup_printf_escaped (format16,"Ranking");
    okno->label_tytul=gtk_label_new("Ranking");
    gtk_label_set_markup (GTK_LABEL(okno->label_tytul), markup);

    markup = g_markup_printf_escaped (format12,"Wybierz dla jakiego rozmiaru planszy i rodzaju gry wyświetlić ranking");
    okno->label_wybierz=gtk_label_new("Wybierz dla jakiego rozmiaru planszy i rodzaju gry wyświetlić ranking");
    gtk_label_set_markup (GTK_LABEL(okno->label_wybierz), markup);

    markup = g_markup_printf_escaped (format12,"Pozycja");
    okno->label_pozycja=gtk_label_new("Pozycja");
    gtk_label_set_markup (GTK_LABEL(okno->label_pozycja), markup);

    markup = g_markup_printf_escaped (format12,"Wynik");
    okno->label_wynik=gtk_label_new("Wynik");
    gtk_label_set_markup (GTK_LABEL(okno->label_wynik), markup);

    markup = g_markup_printf_escaped (format12,"Imię gracza");
    okno->label_imie_gracza=gtk_label_new("Imię gracza");
    gtk_label_set_markup (GTK_LABEL(okno->label_imie_gracza), markup);

    gtk_grid_attach(GTK_GRID(okno->grid), okno->label_tytul, 0, 0, 3, 1);
    gtk_grid_attach(GTK_GRID(okno->grid), okno->label_wybierz, 0, 1, 3, 1);
    gtk_grid_attach(GTK_GRID(okno->grid), okno->combo_box_rozmiar, 0, 2, 2, 2);

    okno->radio1 = gtk_radio_button_new_with_label(NULL, "Gra jednoosobowa");
    okno->czy_multi=0;
    okno->radio2 = gtk_radio_button_new_with_label_from_widget (GTK_RADIO_BUTTON (okno->radio1),"Gra dwuosobowa");

    gtk_grid_attach(GTK_GRID(okno->grid), okno->radio1, 2, 2, 1, 1);
    gtk_grid_attach(GTK_GRID(okno->grid), okno->radio2, 2, 3, 1, 1);

    gtk_grid_attach(GTK_GRID(okno->grid), okno->label_pozycja, 0, 4, 1, 1);
    gtk_grid_attach(GTK_GRID(okno->grid), okno->label_imie_gracza, 1, 4, 1, 1);
    gtk_grid_attach(GTK_GRID(okno->grid), okno->label_wynik, 2, 4, 1, 1);

    for(int i=0; i<10; i++)
    {
        gtk_grid_attach(GTK_GRID(okno->grid), okno->numer[i], 0, i+5, 1, 1);
        gtk_grid_attach(GTK_GRID(okno->grid), okno->imie[i], 1, i+5, 1, 1);
        gtk_grid_attach(GTK_GRID(okno->grid), okno->punkty[i], 2, i+5, 1, 1);
    }
}

void radio_1(GtkWidget *widget,gpointer data)
{
    okno_ranking *temp=(okno_ranking*)data;
    temp->czy_multi=0;

    zmiana_rankingu(widget, data);
}

void radio_2(GtkWidget *widget,gpointer data)
{
    okno_ranking *temp=(okno_ranking*)data;
    temp->czy_multi=1;

    zmiana_rankingu(widget, data);
}
