#include <gtk/gtk.h>
#include "funkcje_dodatkowe.h"

typedef struct pipes *PipesPtr;
GtkWidget *widget_okno_glowne;

PipesPtr initPipes(char znak);
void     sendStringToPipe(PipesPtr channel, const char *data);
bool     getStringFromPipe(PipesPtr channel, char *buffer, size_t size);
void     closePipes(PipesPtr channel);
