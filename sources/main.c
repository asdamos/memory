#include "menu_glowne.h"
#include <gtk/gtk.h>



int main(int argc,char *argv[])
{
    czy_gra_wlaczona=false;
    czy_ranking_wlaczony=false;
    gtk_init (&argc, &argv);

    okno_glowne okno;

    GtkWidget *window=gtk_window_new(GTK_WINDOW_TOPLEVEL);

    widget_okno_glowne=window;
    okno.window=window;
    gtk_window_set_resizable(GTK_WINDOW(window), FALSE);

    utworz_widgety(&okno);

    obsluga_funkcji(&okno);

    gtk_widget_show_all(window);
    gtk_main ();

    return 0;
}
