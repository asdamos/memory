#include <gtk/gtk.h>
#include "fifo.h"


typedef struct okno_gry
{
    GtkWidget *window;
    GtkWidget *box;
    GtkWidget *box_plansza;
    GtkWidget *grid_plansza;
    GtkWidget *grid_wyniki;
    GtkWidget *grid_info;

    GtkWidget *event_box;

    char *imie_gracza;
    char *imie_przeciwnika;
    char *nazwa_tekstur;
    int liczba_tekstur;
    bool czy_tekstury_animowane;

    int wysokosc_planszy;
    int szerokosc_planszy;
    GtkWidget *obrazek;

    GtkWidget *label_nazwa_gracza;
    GtkWidget *label_nazwa_przeciwnika;
    GtkWidget *label_liczba_punktow1;
    GtkWidget *label_liczba_punktow2;
    GtkWidget *label_liczba_punktow_gracza;
    GtkWidget *label_liczba_punktow_przeciwnika;

    int liczba_punktow_gracza;
    int liczba_punktow_przeciwnika;

    int tryb_gracza;   //singleplayer - 0, gospodarz -1, klient - 2

    int **plansza;                  //numery kart
    int **stan_planszy;             //1-jest karta, 0-zabrano karte

    GtkWidget ***tablica_widgetow;

    GdkPixbuf **tablica_pixbufow;
    GdkPixbuf *pixbuf_rewers; GdkPixbuf *pixbuf_pusty;

    int ile_par_zostalo;
    int czy_wyslac_komunikat_o_zamknietym_oknie;

    int kolej_nacisnietego;
    int x_poprzedniej, y_poprzedniej; //-1 gdy nie bylo poprzedniej
    int x_nastepnej, y_nastepnej;

    int kolej_gracza;       //1 - twoja kolej, 0 - kolej przeciwnika
    GtkWidget * label_info_text;
    GtkWidget * label_info;
    PipesPtr * potoki;

}okno_gry;

void rezerwuj_pamiec_gra(okno_gry *okno);

void zwolnij_pamiec_gra(GtkWidget *widget,gpointer data);

void utworz_widgety_gra(okno_gry *okno);

void obsluga_funkcji_gra(okno_gry *okno);

void utworz_okno_gra(okno_gry *okno);

void utworz_boxy_gra(okno_gry *okno);

void utworz_elementy_gra(okno_gry *okno);

int random(int min, int max);

void losuj_ustawienie_kart(okno_gry *okno);

gboolean nacisnieto_plansze(GtkWidget *event_box, GdkEventButton*event, gpointer data);

gboolean odbierz_informacje(gpointer data);

void koniec_gry(okno_gry *okno);
