#include <gtk/gtk.h>
#include "okno_gry.h"




typedef struct okno_glowne
{
    GtkWidget *window;
    GtkWidget *box;  //glowny box
    GtkWidget *box_tytul;
    GtkWidget *box_nazwa_gracza;
    GtkWidget *grid_radio;
    GtkWidget *separator1;
    GtkWidget *separator2;
    GtkWidget *grid_ustawienia;
    GtkWidget *grid_guziki;

    GtkWidget *box_ustawienia_rozmiar;
    GtkWidget *box_ustawienia_tekstury;
    GtkWidget *combo_box_rozmiar;
    GtkWidget *combo_box_tekstury;

    GtkWidget *tytul;
    GtkWidget *obrazek;
    GtkWidget *label_imie;
    GtkWidget *entry;
    GtkWidget *button_graj;
    GtkWidget *radio1;
    GtkWidget *radio2;
    GtkWidget *radio3;

    GtkWidget *label_tekstury;
    GtkWidget *label_rozmiar;

    GtkWidget *button_ranking;
    GtkWidget *button_autor;


    int ile_rozmiarow;
    int **rozmiary_planszy;

    int ile_tekstur;
    char **nazwy_tekstur;
    int *liczba_obrazkow;
    bool *czy_tekstura_animowana;

    int tryb_gracza;   //singleplayer - 0, gospodarz -1, klient - 2
}okno_glowne;


void zwolnij_pamiec(GtkWidget *widget,gpointer data);

void utworz_okno(okno_glowne *okno);

void utworz_boxy(okno_glowne *okno);

void utworz_elementy(okno_glowne *okno);

void utworz_widgety(okno_glowne *okno);

void obsluga_funkcji(okno_glowne *okno);

void graj(GtkWidget *widget,gpointer data);

void radio_g(GtkWidget *widget,gpointer data);

void radio_k(GtkWidget *widget,gpointer data);

void radio_s(GtkWidget *widget,gpointer data);

void o_autorze(GtkWidget *widget,gpointer data);

void wczytaj_tekstury(okno_glowne *okno);

void wczytaj_rozmiary(okno_glowne *okno);

void zwolnij_pamiec(GtkWidget *widget,gpointer data);
