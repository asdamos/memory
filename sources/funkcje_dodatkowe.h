#include <gtk/gtk.h>
#include <stdbool.h>
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>
#include <stdio.h>
#include <time.h>

bool czy_gra_wlaczona;
bool czy_ranking_wlaczony;

typedef struct ranking
{
    int suma_kontrolna;
    int wynik[10];
    char imie[10][100];
}ranking;

typedef struct okno_ranking
{
    GtkWidget *window;
    GtkWidget *box;
    GtkWidget *grid;

    GtkWidget *label_tytul;
    GtkWidget *label_wybierz;
    GtkWidget *label_pozycja;
    GtkWidget *label_imie_gracza;
    GtkWidget *label_wynik;
    GtkWidget *obrazek;
    GtkWidget *radio1;
    GtkWidget *radio2;
    int czy_multi;

    GtkWidget **numer;
    GtkWidget **imie;
    GtkWidget **punkty;

    GtkWidget *combo_box_rozmiar;
    int ile_rozmiarow;
    int **rozmiary_planszy;

}okno_ranking;

void pokaz_blad(char *tekst, GtkWidget *window);

void pokaz_info(char *tekst, GtkWidget * window);

void dopisz_do_rankingu(char *imie, int wynik, ranking *r);

int policz_sume_kontrolna(ranking *r);

ranking *utworz();

ranking *wczytaj(int wysokosc, int szerokosc, int czy_multi);

void dodaj_do_grida(okno_ranking *okno);

void zapisz(int wysokosc, int szerokosc, int czy_multi, ranking*r);

void radio_1(GtkWidget *widget,gpointer data);

void radio_2(GtkWidget *widget,gpointer data);

void wyswietl_ranking(GtkWidget *widget,gpointer data);

void utworz_elementy_ranking(okno_ranking *okno);

void rezerwuj_pamiec_ranking(okno_ranking *okno);

void wczytaj_rozmiary_ranking(okno_ranking *okno);


void zwolnij_pamiec_ranking(GtkWidget *widget,gpointer data);

void zmiana_rankingu(GtkWidget *widget,gpointer data);

