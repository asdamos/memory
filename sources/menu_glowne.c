#include "menu_glowne.h"



void utworz_okno(okno_glowne * okno)
{
    gtk_window_set_title(GTK_WINDOW(okno->window),"Memory");
    gtk_window_set_position(GTK_WINDOW(okno->window),GTK_WIN_POS_CENTER);
    gtk_container_set_border_width(GTK_CONTAINER(okno->window), 10);
}


void utworz_boxy(okno_glowne *okno)
{
    okno->box=gtk_box_new(GTK_ORIENTATION_VERTICAL, 0);
    okno->box_tytul=gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 5);

    okno->box_nazwa_gracza=gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 0);

    okno->box_ustawienia_tekstury=gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 0);
    okno->box_ustawienia_rozmiar=gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 0);

    okno->combo_box_rozmiar=gtk_combo_box_text_new();
    okno->combo_box_tekstury=gtk_combo_box_text_new();

    gtk_container_add(GTK_CONTAINER(okno->window), okno->box);

}

void utworz_elementy(okno_glowne *okno)
{
    okno->tytul=gtk_label_new("Memory");
    const char *format ="<span font_desc=\"28.0\" >%s</span>";
    char *markup;
    markup = g_markup_printf_escaped (format, "Memory");
    gtk_label_set_markup (GTK_LABEL(okno->tytul), markup);
    g_free (markup);

    okno->obrazek=gtk_image_new_from_file("dane/logo.png");
    gtk_window_set_icon(GTK_WINDOW(okno->window), gtk_image_get_pixbuf(GTK_IMAGE(okno->obrazek)));
    gtk_box_set_homogeneous(GTK_BOX(okno->box_tytul), TRUE);
    gtk_box_pack_start(GTK_BOX(okno->box_tytul), okno->tytul, TRUE, TRUE, 0);
    gtk_box_pack_start(GTK_BOX(okno->box_tytul), okno->obrazek, TRUE, TRUE, 0);
    gtk_box_pack_start(GTK_BOX(okno->box), okno->box_tytul, TRUE, TRUE, 10);
    ////
    gtk_box_pack_start(GTK_BOX(okno->box), okno->box_nazwa_gracza, TRUE, TRUE, 0);
    okno->label_imie=gtk_label_new("Imię");
    gtk_box_pack_start(GTK_BOX(okno->box_nazwa_gracza), okno->label_imie, TRUE, TRUE, 0);
    okno->entry=gtk_entry_new();
    gtk_entry_set_text(GTK_ENTRY(okno->entry),"Gość");
    gtk_box_pack_start(GTK_BOX(okno->box_nazwa_gracza), okno->entry, TRUE, TRUE, 0);
    ////

    okno->radio1 = gtk_radio_button_new_with_label(NULL, "Gra jednoosobowa");
    okno->tryb_gracza=0;
    okno->radio2 = gtk_radio_button_new_with_label_from_widget (GTK_RADIO_BUTTON (okno->radio1),"Gra dwuosobowa - serwer");
    okno->radio3 = gtk_radio_button_new_with_label_from_widget (GTK_RADIO_BUTTON (okno->radio1),"Gra dwuosobowa - klient");

    okno->grid_radio=gtk_grid_new();
    gtk_box_pack_start(GTK_BOX(okno->box), okno->grid_radio, TRUE, TRUE, 5);
    gtk_grid_attach(GTK_GRID(okno->grid_radio), okno->radio1, 0, 0, 1, 1);
    gtk_grid_attach(GTK_GRID(okno->grid_radio), okno->radio2, 0, 1, 1, 1);
    gtk_grid_attach(GTK_GRID(okno->grid_radio), okno->radio3, 0, 2, 1, 1);
    gtk_grid_set_column_homogeneous(GTK_GRID(okno->grid_radio), TRUE);
    gtk_grid_set_row_homogeneous(GTK_GRID(okno->grid_radio), TRUE);
    //////
    okno->separator1=gtk_separator_new(GTK_ORIENTATION_HORIZONTAL);
    gtk_box_pack_start(GTK_BOX(okno->box), okno->separator1, TRUE, TRUE, 0);
    //////
    okno->label_rozmiar=gtk_label_new("Rozmiar planszy");
    okno->label_tekstury=gtk_label_new("Zestaw tekstur");
    okno->combo_box_rozmiar=gtk_combo_box_text_new();
    okno->combo_box_tekstury=gtk_combo_box_text_new();

    for(int i=0; i<okno->ile_tekstur; i++)
    {
        gtk_combo_box_text_insert_text(GTK_COMBO_BOX_TEXT(okno->combo_box_tekstury), i, okno->nazwy_tekstur[i]);
    }
    gtk_combo_box_set_active(GTK_COMBO_BOX(okno->combo_box_tekstury), 0);

    for(int i=0; i<okno->ile_rozmiarow; i++)
    {
        char text[50];
        sprintf(text, "%dx%d", okno->rozmiary_planszy[i][0], okno->rozmiary_planszy[i][1]);
        gtk_combo_box_text_insert_text(GTK_COMBO_BOX_TEXT(okno->combo_box_rozmiar), i, text);
    }
    gtk_combo_box_set_active(GTK_COMBO_BOX(okno->combo_box_rozmiar), 0);

    okno->grid_ustawienia=gtk_grid_new();
    gtk_box_pack_start(GTK_BOX(okno->box), okno->grid_ustawienia, TRUE, TRUE, 5);

    gtk_grid_attach(GTK_GRID(okno->grid_ustawienia), okno->label_rozmiar, 0, 0, 1, 1);
    gtk_grid_attach(GTK_GRID(okno->grid_ustawienia), okno->combo_box_rozmiar, 1, 0, 1, 1);
    gtk_grid_attach(GTK_GRID(okno->grid_ustawienia), okno->label_tekstury, 0, 1, 1, 1);
    gtk_grid_attach(GTK_GRID(okno->grid_ustawienia), okno->combo_box_tekstury, 1, 1, 1, 1);
    gtk_grid_set_column_homogeneous(GTK_GRID(okno->grid_ustawienia), TRUE);
    gtk_grid_set_row_homogeneous(GTK_GRID(okno->grid_ustawienia), TRUE);
    /////
    okno->separator2=gtk_separator_new(GTK_ORIENTATION_HORIZONTAL);
    gtk_box_pack_start(GTK_BOX(okno->box), okno->separator2, TRUE, TRUE, 0);
    ////
    okno->grid_guziki=gtk_grid_new();
    gtk_box_pack_start(GTK_BOX(okno->box), okno->grid_guziki, TRUE, TRUE, 5);

    okno->button_graj=gtk_button_new_with_label("Graj");
    okno->button_ranking=gtk_button_new_with_label("Ranking");
    okno->button_autor=gtk_button_new_with_label("Autor");

    gtk_grid_attach(GTK_GRID(okno->grid_guziki), okno->button_graj, 0, 0, 2, 1);
    gtk_grid_attach(GTK_GRID(okno->grid_guziki), okno->button_ranking, 0, 1, 1, 1);
    gtk_grid_attach(GTK_GRID(okno->grid_guziki), okno->button_autor, 1, 1, 1, 1);

    gtk_grid_set_column_homogeneous(GTK_GRID(okno->grid_guziki), TRUE);
    gtk_grid_set_row_homogeneous(GTK_GRID(okno->grid_guziki), TRUE);

}

void o_autorze(GtkWidget *widget,gpointer data)
{
    GtkWidget *dialog = gtk_about_dialog_new();
    GtkWidget *obrazek=gtk_image_new_from_file("dane/logo.png");
    gtk_window_set_icon(GTK_WINDOW(dialog), gtk_image_get_pixbuf(GTK_IMAGE(obrazek)));
    gtk_window_set_position(GTK_WINDOW(dialog),GTK_WIN_POS_CENTER);

    gtk_about_dialog_set_program_name(GTK_ABOUT_DIALOG(dialog), "Memory");
    gtk_about_dialog_set_version(GTK_ABOUT_DIALOG(dialog), "Wersja Beta 1.0");
    gtk_about_dialog_set_copyright(GTK_ABOUT_DIALOG(dialog), "Copyright © 2015 Adam Sawicki");
    gtk_about_dialog_set_comments (GTK_ABOUT_DIALOG(dialog), "Gra wykonana jako projekt na pracownie C na rok 2014-15\nNumer indeksu: 270814");
    gtk_about_dialog_set_website_label(GTK_ABOUT_DIALOG(dialog), "O różnych odmianach na wikipedii");
    gtk_about_dialog_set_website(GTK_ABOUT_DIALOG(dialog), "http://en.wikipedia.org/wiki/Concentration_%28game%29");
    gtk_about_dialog_set_logo (GTK_ABOUT_DIALOG(dialog), gtk_image_get_pixbuf(GTK_IMAGE(obrazek)));
    gtk_dialog_run(GTK_DIALOG(dialog));
    gtk_widget_destroy(dialog);
}

void utworz_widgety(okno_glowne *okno)
{
    utworz_okno(okno);
    wczytaj_tekstury(okno);
    wczytaj_rozmiary(okno);
    utworz_boxy(okno);
    utworz_elementy(okno);

}


void zwolnij_pamiec(GtkWidget *widget,gpointer data)
{
    okno_glowne *temp=(okno_glowne*)data;

    for(int i=0; i<temp->ile_rozmiarow; i++)
    {
        free(temp->rozmiary_planszy[i]);
    }
    free(temp->rozmiary_planszy);

    for(int i=0; i<temp->ile_tekstur; i++)
    {
        free(temp->nazwy_tekstur[i]);
    }
    free(temp->liczba_obrazkow);
    free(temp->czy_tekstura_animowana);
    free(temp->nazwy_tekstur);

    printf("ZWOLNIONO PAMIEC Z OKNA GLOWNEGO\n");
    gtk_main_quit();
}

void graj(GtkWidget *widget,gpointer data)
{
    okno_glowne *temp=(okno_glowne*)data;
    int pomocnik;
    static PipesPtr potoki;
    if(czy_gra_wlaczona==true)
    {
        return;
    }

    if(temp->tryb_gracza==1 || temp->tryb_gracza==0)
    {

        pomocnik=gtk_combo_box_get_active(GTK_COMBO_BOX(temp->combo_box_rozmiar));
        int ilosc_pol;
        ilosc_pol=temp->rozmiary_planszy[pomocnik][0]*temp->rozmiary_planszy[pomocnik][1];

        pomocnik=gtk_combo_box_get_active(GTK_COMBO_BOX(temp->combo_box_tekstury));
        if(ilosc_pol>(temp->liczba_obrazkow[pomocnik]*2))
        {
            pokaz_blad("Wybrany zestaw tekstur zawiera zbyt mało obrazków.\nProszę wybrać inny zestaw albo zmniejszyć rozmiar planszy.",temp->window);
            return;
        }


        if(temp->tryb_gracza==1)
        {
            if ((potoki=initPipes('A')) == NULL)
            {
                return;
            }
        }

    }

    if(temp->tryb_gracza==2)
    {
        if ((potoki=initPipes('B')) == NULL)
        {
            return;
        }
    }
    GtkWidget *window=gtk_window_new(GTK_WINDOW_TOPLEVEL);  //okno planszy

    gtk_window_set_resizable(GTK_WINDOW(window), FALSE);
    okno_gry *okno=malloc(sizeof(okno_gry));

    if(temp->tryb_gracza!=0)    //nie jest uruchomiony singleplayer
    {
        okno->potoki=&potoki;
    }
    czy_gra_wlaczona=true;

    okno->window=window;
    okno->imie_gracza=(char*)gtk_entry_get_text(GTK_ENTRY(temp->entry));
    okno->tryb_gracza=temp->tryb_gracza;




    int rozmiar_bufora=100;
    char tekst[rozmiar_bufora];
    if(temp->tryb_gracza==0)    //single
    {
        pomocnik=gtk_combo_box_get_active(GTK_COMBO_BOX(temp->combo_box_tekstury));
        okno->liczba_tekstur=temp->liczba_obrazkow[pomocnik];

        okno->nazwa_tekstur=temp->nazwy_tekstur[pomocnik];

        okno->czy_tekstury_animowane=temp->czy_tekstura_animowana[pomocnik];

        pomocnik=gtk_combo_box_get_active(GTK_COMBO_BOX(temp->combo_box_rozmiar));
        okno->wysokosc_planszy=temp->rozmiary_planszy[pomocnik][0];
        okno->szerokosc_planszy=temp->rozmiary_planszy[pomocnik][1];
        okno->kolej_gracza=1;
    }
    if(temp->tryb_gracza==1)    //host
    {
        okno->kolej_gracza=random(0, 2);
        if(okno->kolej_gracza==1)
            sendStringToPipe(potoki, "0");
        else
            sendStringToPipe(potoki, "1");

        pomocnik=gtk_combo_box_get_active(GTK_COMBO_BOX(temp->combo_box_tekstury));
        okno->liczba_tekstur=temp->liczba_obrazkow[pomocnik];

        okno->nazwa_tekstur=temp->nazwy_tekstur[pomocnik];

        okno->czy_tekstury_animowane=temp->czy_tekstura_animowana[pomocnik];

        sprintf(tekst, "%d", pomocnik);
        sendStringToPipe(potoki, tekst);


        pomocnik=gtk_combo_box_get_active(GTK_COMBO_BOX(temp->combo_box_rozmiar));
        okno->wysokosc_planszy=temp->rozmiary_planszy[pomocnik][0];
        okno->szerokosc_planszy=temp->rozmiary_planszy[pomocnik][1];

        sprintf(tekst, "%d", pomocnik);
        sendStringToPipe(potoki, tekst);

        sendStringToPipe(potoki, okno->imie_gracza);
        while (!getStringFromPipe(potoki, tekst, rozmiar_bufora));
        okno->imie_przeciwnika=tekst;
        printf("WYSLANO USTAWIENIA\n");
    }
    if(temp->tryb_gracza==2)    //gosc
    {
        while (!getStringFromPipe(potoki, tekst, rozmiar_bufora));
        okno->kolej_gracza=atoi(tekst);

        while (!getStringFromPipe(potoki, tekst, rozmiar_bufora));
        pomocnik=atoi(tekst);
        okno->liczba_tekstur=temp->liczba_obrazkow[pomocnik];

        okno->nazwa_tekstur=temp->nazwy_tekstur[pomocnik];

        okno->czy_tekstury_animowane=temp->czy_tekstura_animowana[pomocnik];


        while (!getStringFromPipe(potoki, tekst, rozmiar_bufora));
        pomocnik=atoi(tekst);
        okno->wysokosc_planszy=temp->rozmiary_planszy[pomocnik][0];
        okno->szerokosc_planszy=temp->rozmiary_planszy[pomocnik][1];


        while (!getStringFromPipe(potoki, tekst, rozmiar_bufora));
        okno->imie_przeciwnika=tekst;

        sendStringToPipe(potoki, okno->imie_gracza);
        printf("ODEBRANO USTAWIENIA\n");
    }
    utworz_widgety_gra(okno);

    rezerwuj_pamiec_gra(okno);


    if(temp->tryb_gracza==0)
    {
        for(int i=0; i<okno->wysokosc_planszy; i++)
        {
            for(int j=0; j<okno->szerokosc_planszy; j++)
            {
                okno->plansza[i][j]=-1;
            }
        }
        losuj_ustawienie_kart(okno);
    }
    if(temp->tryb_gracza==1)    //wyslanie planszy
    {
        for(int i=0; i<okno->wysokosc_planszy; i++)
        {
            for(int j=0; j<okno->szerokosc_planszy; j++)
            {
                okno->plansza[i][j]=-1;
            }
        }
        losuj_ustawienie_kart(okno);

        for(int i=0; i<okno->wysokosc_planszy; i++)
        {
            for(int j=0; j<okno->szerokosc_planszy; j++)
            {
                sprintf(tekst, "%d", okno->plansza[i][j]);
                sendStringToPipe(potoki, tekst);
            }
        }
        printf("WYSLANO PLANSZE\n");
    }
    if(temp->tryb_gracza==2)    //odebranie planszy
    {
        for(int i=0; i<okno->wysokosc_planszy; i++)
        {
            for(int j=0; j<okno->szerokosc_planszy; j++)
            {
                while (!getStringFromPipe(potoki, tekst, rozmiar_bufora));
                okno->plansza[i][j]=atoi(tekst);
                //printf("%d ", okno->plansza[i][j]);
            }
            //printf("\n");
        }
        printf("ODEBRANO PLANSZE\n");
    }
    obsluga_funkcji_gra(okno);
    gtk_widget_show_all(window);
}

void radio_g(GtkWidget *widget,gpointer data)
{
    okno_glowne *temp=(okno_glowne*)data;
    temp->tryb_gracza=1;

    gtk_widget_show(temp->grid_ustawienia);
}

void radio_k(GtkWidget *widget,gpointer data)
{
    okno_glowne *temp=(okno_glowne*)data;
    temp->tryb_gracza=2;

    gtk_widget_hide(temp->grid_ustawienia);
}

void radio_s(GtkWidget *widget,gpointer data)
{
    okno_glowne *temp=(okno_glowne*)data;
    temp->tryb_gracza=0;

    gtk_widget_show(temp->grid_ustawienia);
}


void obsluga_funkcji(okno_glowne * okno)
{
    g_signal_connect(G_OBJECT(okno->radio1), "clicked", G_CALLBACK(radio_s), (gpointer) okno);
    g_signal_connect(G_OBJECT(okno->radio2), "clicked", G_CALLBACK(radio_g), (gpointer) okno);
    g_signal_connect(G_OBJECT(okno->radio3), "clicked", G_CALLBACK(radio_k), (gpointer) okno);
    g_signal_connect(G_OBJECT(okno->button_autor), "clicked", G_CALLBACK(o_autorze), (gpointer) okno);
    g_signal_connect(G_OBJECT(okno->button_graj), "clicked", G_CALLBACK(graj), (gpointer) okno);
    g_signal_connect(G_OBJECT(okno->button_ranking), "clicked", G_CALLBACK(wyswietl_ranking), (gpointer) okno);
    g_signal_connect(G_OBJECT(okno->window),"destroy",G_CALLBACK(zwolnij_pamiec),(gpointer) okno);
}

void wczytaj_tekstury(okno_glowne *okno)
{
    printf("WCZYTUJE TEKSTURY\n");
    FILE *plik;
    plik=fopen("dane/tekstury.txt", "r");
    char tekst[100];
    int pomocnik;
    if(plik==NULL)
        exit(666);
    fscanf(plik, "%d", &okno->ile_tekstur);
    printf("%d\n", okno->ile_tekstur);


    okno->liczba_obrazkow=malloc(sizeof(int)*okno->ile_tekstur);
    okno->czy_tekstura_animowana=malloc(sizeof(bool)*okno->ile_tekstur);
    okno->nazwy_tekstur=malloc(sizeof(char*)*okno->ile_tekstur);
    for(int i=0; i<okno->ile_tekstur; i++)
    {
        fscanf(plik, "%100s", tekst);
        okno->nazwy_tekstur[i]=malloc(sizeof(char)*100);
        strcpy(okno->nazwy_tekstur[i],tekst);
        printf("%s ", okno->nazwy_tekstur[i]);
        fscanf(plik, "%d %d", &okno->liczba_obrazkow[i], &pomocnik);
        printf("%d %d\n", okno->liczba_obrazkow[i], pomocnik);
        if(pomocnik==0)
            okno->czy_tekstura_animowana[i]=false;
        else
            okno->czy_tekstura_animowana[i]=true;
    }
    fclose(plik);
}

void wczytaj_rozmiary(okno_glowne *okno)
{
    printf("WCZYTUJE ROZMIARY\n");
    FILE *plik;
    plik=fopen("dane/rozmiary.txt", "r");
    if(plik==NULL)
        exit(666);
    fscanf(plik, "%d", &okno->ile_rozmiarow);
    printf("%d\n", okno->ile_rozmiarow);
    okno->rozmiary_planszy=malloc(sizeof(int*)*okno->ile_rozmiarow);

    for(int i=0; i<okno->ile_rozmiarow; i++)
    {
        okno->rozmiary_planszy[i]=malloc(sizeof(int)*2);
        fscanf(plik, "%d %d", &okno->rozmiary_planszy[i][0], &okno->rozmiary_planszy[i][1]);
        printf("%d %d\n", okno->rozmiary_planszy[i][0], okno->rozmiary_planszy[i][1]);
    }
    fclose(plik);
}
