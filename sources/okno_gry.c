#include "okno_gry.h"


void utworz_widgety_gra(okno_gry *okno)
{
    utworz_okno_gra(okno);
    utworz_boxy_gra(okno);
    utworz_elementy_gra(okno);
}

void utworz_okno_gra(okno_gry *okno)
{
    char text[100];
    sprintf(text, "Nowa gra - %s", okno->imie_gracza);
    gtk_window_set_title(GTK_WINDOW(okno->window),text);
    gtk_window_set_position(GTK_WINDOW(okno->window),GTK_WIN_POS_CENTER);
    gtk_container_set_border_width(GTK_CONTAINER(okno->window), 10);
}

void utworz_boxy_gra(okno_gry *okno)
{
    okno->box=gtk_box_new(GTK_ORIENTATION_VERTICAL, 0);
    gtk_container_add(GTK_CONTAINER(okno->window), okno->box);
    okno->box_plansza=gtk_box_new(GTK_ORIENTATION_VERTICAL, 0);
    okno->event_box=gtk_event_box_new();
}

void utworz_elementy_gra(okno_gry *okno)
{
    okno->czy_wyslac_komunikat_o_zamknietym_oknie=1;
    okno->obrazek=gtk_image_new_from_file("dane/logo.png");
    gtk_window_set_icon(GTK_WINDOW(okno->window), gtk_image_get_pixbuf(GTK_IMAGE(okno->obrazek)));
    const char *format ="<span font_desc=\"11.0\" >%s</span>";
    char *markup;
    okno->label_nazwa_gracza=gtk_label_new(okno->imie_gracza);
    markup = g_markup_printf_escaped (format,okno->imie_gracza);
    gtk_label_set_markup (GTK_LABEL(okno->label_nazwa_gracza), markup);
    okno->liczba_punktow_gracza=0;

    okno->label_liczba_punktow1=gtk_label_new("Liczba punktów");
    markup = g_markup_printf_escaped (format,"Liczba punktów");
    gtk_label_set_markup (GTK_LABEL(okno->label_liczba_punktow1), markup);
    char bufor[100];

    sprintf(bufor, "%d", okno->liczba_punktow_gracza);
    okno->label_liczba_punktow_gracza=gtk_label_new(bufor);
    markup = g_markup_printf_escaped (format, bufor);
    gtk_label_set_markup (GTK_LABEL(okno->label_liczba_punktow_gracza), markup);

    if(okno->tryb_gracza!=0)
    {
        okno->label_nazwa_przeciwnika=gtk_label_new(okno->imie_przeciwnika);
        markup = g_markup_printf_escaped (format,okno->imie_przeciwnika);
        gtk_label_set_markup (GTK_LABEL(okno->label_nazwa_przeciwnika), markup);
        okno->liczba_punktow_przeciwnika=0;
        okno->label_liczba_punktow2=gtk_label_new("Liczba punktów");
        markup = g_markup_printf_escaped (format,"Liczba punktów");
        gtk_label_set_markup (GTK_LABEL(okno->label_liczba_punktow2), markup);

        sprintf(bufor, "%d", okno->liczba_punktow_przeciwnika);
        okno->label_liczba_punktow_przeciwnika=gtk_label_new(bufor);
        markup = g_markup_printf_escaped (format, bufor);
        gtk_label_set_markup (GTK_LABEL(okno->label_liczba_punktow_przeciwnika), markup);
    }


    okno->grid_wyniki=gtk_grid_new();
    okno->grid_plansza=gtk_grid_new();
    okno->grid_info=gtk_grid_new();

    gtk_box_pack_start(GTK_BOX(okno->box), okno->grid_wyniki, TRUE, TRUE, 0);

    gtk_grid_set_column_homogeneous(GTK_GRID(okno->grid_wyniki), TRUE);
    gtk_grid_set_column_homogeneous(GTK_GRID(okno->grid_info), TRUE);
    gtk_grid_set_row_homogeneous(GTK_GRID(okno->grid_plansza), TRUE);
    gtk_grid_set_row_spacing(GTK_GRID(okno->grid_plansza), 5);
    gtk_grid_set_column_spacing(GTK_GRID(okno->grid_plansza), 5);

    if(okno->tryb_gracza!=0)
    {
        gtk_grid_attach(GTK_GRID(okno->grid_wyniki), okno->label_nazwa_gracza, 0, 0, 2, 1);
        gtk_grid_attach(GTK_GRID(okno->grid_wyniki), okno->label_nazwa_przeciwnika, 2, 0, 2, 1);
        gtk_grid_attach(GTK_GRID(okno->grid_wyniki), okno->label_liczba_punktow1, 0, 1, 1, 1);
        gtk_grid_attach(GTK_GRID(okno->grid_wyniki), okno->label_liczba_punktow_gracza, 1, 1, 1, 1);
        gtk_grid_attach(GTK_GRID(okno->grid_wyniki), okno->label_liczba_punktow2, 2, 1, 1, 1);
        gtk_grid_attach(GTK_GRID(okno->grid_wyniki), okno->label_liczba_punktow_przeciwnika, 3, 1, 1, 1);


        okno->label_info_text=gtk_label_new("Trwa kolej gracza");
        markup = g_markup_printf_escaped (format,"Trwa kolej gracza");
        gtk_label_set_markup (GTK_LABEL(okno->label_info_text), markup);


        if(okno->kolej_gracza==1)
        {
            okno->label_info=gtk_label_new(okno->imie_gracza);
            markup = g_markup_printf_escaped (format, okno->imie_gracza);
            gtk_label_set_markup (GTK_LABEL(okno->label_info), markup);
        }
        else
        {
            okno->label_info=gtk_label_new(okno->imie_przeciwnika);
            markup = g_markup_printf_escaped (format, okno->imie_przeciwnika);
            gtk_label_set_markup (GTK_LABEL(okno->label_info), markup);
        }



        gtk_grid_attach(GTK_GRID(okno->grid_info), okno->label_info_text, 0, 0, 1, 1);
        gtk_grid_attach(GTK_GRID(okno->grid_info), okno->label_info, 1, 0, 1, 1);
    }
    else
    {
        gtk_grid_attach(GTK_GRID(okno->grid_wyniki), okno->label_nazwa_gracza, 0, 0, 2, 1);
        gtk_grid_attach(GTK_GRID(okno->grid_wyniki), okno->label_liczba_punktow1, 0, 1, 1, 1);
        gtk_grid_attach(GTK_GRID(okno->grid_wyniki), okno->label_liczba_punktow_gracza, 1, 1, 1, 1);
    }


    ///////////////////////////////////////////////
    gtk_container_add(GTK_CONTAINER(okno->box), okno->box_plansza);
    gtk_container_add(GTK_CONTAINER(okno->box_plansza), okno->event_box);
    gtk_container_add(GTK_CONTAINER(okno->event_box), okno->grid_plansza);
    gtk_container_add(GTK_CONTAINER(okno->box), okno->grid_info);



    okno->x_poprzedniej=-1;
    okno->y_poprzedniej=-1;
    okno->ile_par_zostalo=okno->szerokosc_planszy*okno->wysokosc_planszy/2;
    okno->kolej_nacisnietego=0;
    g_free (markup);
}

void obsluga_funkcji_gra(okno_gry *okno)
{
    g_signal_connect(G_OBJECT(okno->event_box), "button_press_event", G_CALLBACK(nacisnieto_plansze), (gpointer) okno);
    g_signal_connect(G_OBJECT(okno->window),"destroy",G_CALLBACK(zwolnij_pamiec_gra),(gpointer) okno);
    if(okno->tryb_gracza!=0)
    {
        g_timeout_add(100, odbierz_informacje, (gpointer) okno);
    }

}

gboolean nacisnieto_plansze(GtkWidget *event_box, GdkEventButton*event, gpointer data)
{
    okno_gry *temp=(okno_gry*)data;

    if(temp->kolej_gracza==0)       //kolej przeciwnika
    {
        return true;
    }

    if(temp->ile_par_zostalo==0)
        return true;

    int x, y;
    x=event->x/(100.0+5);
    y=event->y/(100.0+5);

    if(x>temp->szerokosc_planszy-1 || y>temp->wysokosc_planszy-1)
        return false;
    printf("Wybrano obrazek o wspolrzednych x:%d y:%d\n", x, y);
    char bufor[100];

    const char *format ="<span font_desc=\"11.0\" >%s</span>";
    char *markup;

    if(temp->kolej_nacisnietego==2)     //konczymy nasz ruch
    {
        if(temp->tryb_gracza!=0)
        {
            sprintf(bufor, "%d %d", -5, -5);
            sendStringToPipe(*temp->potoki, bufor);
            temp->kolej_gracza=0;
            markup = g_markup_printf_escaped (format,gtk_label_get_text(GTK_LABEL(temp->label_nazwa_przeciwnika)));
            gtk_label_set_markup (GTK_LABEL(temp->label_info), markup);

        }

        gtk_image_set_from_pixbuf(GTK_IMAGE(temp->tablica_widgetow[temp->y_nastepnej][temp->x_nastepnej]), temp->pixbuf_rewers);
        gtk_image_set_from_pixbuf(GTK_IMAGE(temp->tablica_widgetow[temp->y_poprzedniej][temp->x_poprzedniej]), temp->pixbuf_rewers);

        temp->x_poprzedniej=-1;
        temp->y_poprzedniej=-1;

        temp->kolej_nacisnietego=0;

        return true;
    }


    if(temp->stan_planszy[y][x]==0)     //nie ma tam karty
    {
        return true;
    }

    if(temp->kolej_nacisnietego==0)      //nie wybrano jeszcze zadnej karty
    {
        temp->x_poprzedniej=x;
        temp->y_poprzedniej=y;

        if(temp->tryb_gracza!=0)
        {
            sprintf(bufor, "%d %d", x, y);
            sendStringToPipe(*temp->potoki, bufor);
        }
        gtk_image_set_from_pixbuf(GTK_IMAGE(temp->tablica_widgetow[y][x]), temp->tablica_pixbufow[temp->plansza[y][x]]);
        temp->kolej_nacisnietego=1;
        return true;
    }

    //odkryto juz jakas karte wczesniej

    if(temp->x_poprzedniej == x && temp->y_poprzedniej == y)     //nacisnieto ta sama
        return true;


    // odkryto nowa


    if(temp->tryb_gracza!=0)
    {
        sprintf(bufor, "%d %d", x, y);
        sendStringToPipe(*temp->potoki, bufor);
    }

    if(temp->plansza[y][x] == temp->plansza[temp->y_poprzedniej][temp->x_poprzedniej])      //znaleziono pare
    {
        temp->liczba_punktow_gracza+=10;
        sprintf(bufor, "%d", temp->liczba_punktow_gracza);

        markup = g_markup_printf_escaped (format,bufor);
        gtk_label_set_markup (GTK_LABEL(temp->label_liczba_punktow_gracza), markup);

        temp->stan_planszy[y][x]=0;
        temp->stan_planszy[temp->y_poprzedniej][temp->x_poprzedniej]=0;

        gtk_image_set_from_pixbuf(GTK_IMAGE(temp->tablica_widgetow[y][x]), temp->pixbuf_pusty);
        gtk_image_set_from_pixbuf(GTK_IMAGE(temp->tablica_widgetow[temp->y_poprzedniej][temp->x_poprzedniej]), temp->pixbuf_pusty);

        temp->x_poprzedniej=-1;
        temp->y_poprzedniej=-1;

        temp->kolej_nacisnietego=0;
        temp->ile_par_zostalo--;


        if(temp->ile_par_zostalo==0)
        {
            temp->czy_wyslac_komunikat_o_zamknietym_oknie=0;
            koniec_gry(temp);
        }

        if(temp->tryb_gracza!=0)
        {
            //temp->kolej_gracza=0;
        }
    }
    else                                                                                    //nie znaleziono pary
    {
        temp->x_nastepnej=x;
        temp->y_nastepnej=y;

        gtk_image_set_from_pixbuf(GTK_IMAGE(temp->tablica_widgetow[y][x]), temp->tablica_pixbufow[temp->plansza[y][x]]);

        temp->liczba_punktow_gracza-=1;
        sprintf(bufor, "%d", temp->liczba_punktow_gracza);
        markup = g_markup_printf_escaped (format,bufor);
        gtk_label_set_markup (GTK_LABEL(temp->label_liczba_punktow_gracza), markup);

        temp->kolej_nacisnietego=2;
    }

    return true;
}


gboolean odbierz_informacje(gpointer data)
{
    okno_gry *temp=(okno_gry*)data;

    if(!czy_gra_wlaczona || temp->tryb_gracza==0)
    {
        return false;
    }


    const char *format ="<span font_desc=\"11.0\" >%s</span>";
    char *markup;

    int rozmiar_bufora=100;
    char tekst[rozmiar_bufora];
    int x, y;
    if(getStringFromPipe(*temp->potoki, tekst, rozmiar_bufora))   //odebrano wiadomosc
    {
        sscanf(tekst, "%d %d", &x, &y);
        if(x==-2 && y==-2)       //moja kolej i odebralem wiadomosc - drugi gracz wyszedl
        {
            pokaz_blad("Drugi gracz opuścił grę", temp->window);
            temp->czy_wyslac_komunikat_o_zamknietym_oknie=0;
            gtk_widget_destroy(temp->window);       //niszczymy okno i zwalniamy pamiec
            return true;
        }


        if(temp->kolej_nacisnietego==2)     //konczy sie kolej przeciwnika
        {
            gtk_image_set_from_pixbuf(GTK_IMAGE(temp->tablica_widgetow[temp->y_nastepnej][temp->x_nastepnej]), temp->pixbuf_rewers);
            gtk_image_set_from_pixbuf(GTK_IMAGE(temp->tablica_widgetow[temp->y_poprzedniej][temp->x_poprzedniej]), temp->pixbuf_rewers);

            temp->x_poprzedniej=-1;
            temp->y_poprzedniej=-1;

            temp->kolej_nacisnietego=0;
            temp->kolej_gracza=1;
            markup = g_markup_printf_escaped (format,temp->imie_gracza);
            gtk_label_set_markup (GTK_LABEL(temp->label_info), markup);
            return true;
        }

        if(temp->kolej_nacisnietego==0)      //pierwszy ruch przeciwnika
        {
            temp->x_poprzedniej=x;
            temp->y_poprzedniej=y;

            gtk_image_set_from_pixbuf(GTK_IMAGE(temp->tablica_widgetow[y][x]), temp->tablica_pixbufow[temp->plansza[y][x]]);
            temp->kolej_nacisnietego=1;
            return true;
        }

        //drugi ruch przeciwnika
        if(temp->plansza[y][x] == temp->plansza[temp->y_poprzedniej][temp->x_poprzedniej])      //przeciwnik znalezl pare
        {
            temp->liczba_punktow_przeciwnika+=10;
            sprintf(tekst, "%d", temp->liczba_punktow_przeciwnika);
            markup = g_markup_printf_escaped (format,tekst);
            gtk_label_set_markup (GTK_LABEL(temp->label_liczba_punktow_przeciwnika), markup);

            temp->stan_planszy[y][x]=0;
            temp->stan_planszy[temp->y_poprzedniej][temp->x_poprzedniej]=0;

            gtk_image_set_from_pixbuf(GTK_IMAGE(temp->tablica_widgetow[y][x]), temp->pixbuf_pusty);
            gtk_image_set_from_pixbuf(GTK_IMAGE(temp->tablica_widgetow[temp->y_poprzedniej][temp->x_poprzedniej]), temp->pixbuf_pusty);
            temp->x_poprzedniej=-1;
            temp->y_poprzedniej=-1;

            //temp->kolej_gracza=1;
            temp->kolej_nacisnietego=0;
            temp->ile_par_zostalo--;

            if(temp->ile_par_zostalo==0)
            {
                temp->czy_wyslac_komunikat_o_zamknietym_oknie=0;
                koniec_gry(temp);
            }
        }
        else                                                                                    //przeciwnik nie znalezl pary
        {
            temp->x_nastepnej=x;
            temp->y_nastepnej=y;

            gtk_image_set_from_pixbuf(GTK_IMAGE(temp->tablica_widgetow[y][x]), temp->tablica_pixbufow[temp->plansza[y][x]]);

            temp->liczba_punktow_przeciwnika-=1;
            sprintf(tekst, "%d", temp->liczba_punktow_przeciwnika);
            markup = g_markup_printf_escaped (format,tekst);
            gtk_label_set_markup (GTK_LABEL(temp->label_liczba_punktow_przeciwnika), markup);

            temp->kolej_nacisnietego=2;
        }
        return true;
    }

    return true;
}

////////////////////////////


int random(int min, int max)
{
    int tmp;
    if (max>=min)
        max-= min;
    else
    {
        tmp= min - max;
        min= max;
        max= tmp;
    }
    return max ? (rand() % max + min) : min;
}

void losuj_ustawienie_kart(okno_gry *okno)
{
    srand(time(NULL));
    int n=okno->szerokosc_planszy * okno->wysokosc_planszy;

    int tab[okno->liczba_tekstur];

    for(int i=0; i<okno->liczba_tekstur; i++)
    {
        tab[i]=0;
    }
    int nr_obrazka;
    int x, y;
    for(int i=0; i<n/2; i++)
    {
        do
        {
            nr_obrazka=random(0, okno->liczba_tekstur);
        }
        while(tab[nr_obrazka]!=0);
        tab[nr_obrazka]=1;
        for(int j=0; j<2; j++)
        {
            do
            {
                y=random(0, okno->wysokosc_planszy);
                x=random(0, okno->szerokosc_planszy);
            }
            while(okno->plansza[y][x]!=-1);

            okno->plansza[y][x]=nr_obrazka;
        }
    }
    printf("WYLOSOWANO OBRAZKI\n");
}

void rezerwuj_pamiec_gra(okno_gry *okno)
{
    char bufor[100];

    okno->tablica_pixbufow=malloc(sizeof(GdkPixbuf*)*okno->liczba_tekstur);
    for(int i=0; i<okno->liczba_tekstur; i++)
    {
        GError *blad=NULL;
        okno->tablica_pixbufow[i]=malloc(sizeof(GdkPixbuf*));
        sprintf(bufor, "obrazki/%s/%d.png", okno->nazwa_tekstur, i);
        okno->tablica_pixbufow[i]=gdk_pixbuf_new_from_file_at_scale(bufor, 100, 100, FALSE, &blad);
        if(!okno->tablica_pixbufow[i])
        {
            printf("Error: %s\n", blad->message);
            g_error_free (blad);
        }
    }
    okno->pixbuf_pusty=gdk_pixbuf_new_from_file_at_scale("dane/pusty.png", 100, 100, FALSE, NULL);
    sprintf(bufor, "obrazki/%s/rewers.png", okno->nazwa_tekstur);
    okno->pixbuf_rewers=gdk_pixbuf_new_from_file_at_scale(bufor, 100, 100, FALSE, NULL);


    okno->plansza=malloc(sizeof(int*)*(okno->wysokosc_planszy));
    okno->stan_planszy=malloc(sizeof(int*)*(okno->wysokosc_planszy));
    okno->tablica_widgetow=malloc(sizeof(GtkWidget*)*(okno->wysokosc_planszy));
    for(int i=0; i<okno->wysokosc_planszy; i++)
    {
        okno->plansza[i]=malloc(sizeof(int)*(okno->szerokosc_planszy));
        okno->stan_planszy[i]=malloc(sizeof(int)*(okno->szerokosc_planszy));
        okno->tablica_widgetow[i]=malloc(sizeof(GtkWidget*)*(okno->szerokosc_planszy));
    }

    for(int i=0; i<okno->wysokosc_planszy; i++)
    {
        for(int j=0; j<okno->szerokosc_planszy; j++)
        {
            okno->stan_planszy[i][j]=1;
            okno->tablica_widgetow[i][j]=malloc(sizeof(GtkWidget));

            okno->tablica_widgetow[i][j]=gtk_image_new_from_pixbuf(okno->pixbuf_rewers);
            gtk_grid_attach(GTK_GRID(okno->grid_plansza), okno->tablica_widgetow[i][j], j, i, 1, 1);
        }
    }
    printf("ZAREZERWOWANO PAMIEC\n");
}

void zwolnij_pamiec_gra(GtkWidget *widget,gpointer data)
{
    czy_gra_wlaczona=false;
    okno_gry *temp=(okno_gry*)data;

    if(temp->czy_wyslac_komunikat_o_zamknietym_oknie==1 && temp->tryb_gracza!=0) //wyslanie komunikatu
    {
        sendStringToPipe(*temp->potoki, "-2 -2");
    }

    for(int i=temp->wysokosc_planszy-1; i>=0; i--)
    {
        for(int j=temp->szerokosc_planszy-1; j>=0; j--)
        {
            gtk_widget_destroy(temp->tablica_widgetow[i][j]);
        }
    }
    for(int i=temp->wysokosc_planszy-1; i>=0; i--)
    {
        free(temp->plansza[i]);
        free(temp->stan_planszy[i]);
        free(temp->tablica_widgetow[i]);
    }
    free(temp->plansza);
    free(temp->stan_planszy);
    free(temp->tablica_widgetow);

    for(int i=temp->liczba_tekstur-1; i>=0; i--)
    {
        g_object_unref(temp->tablica_pixbufow[i]);
        //free(temp->tablica_pixbufow[i]);
    }
    free(temp->tablica_pixbufow);
    if(temp->tryb_gracza!=0)
    {
        closePipes(*temp->potoki);
    }



    free(temp);


    printf("ZWOLNIONO PAMIEC Z OKNA GRY\n");
}

void koniec_gry(okno_gry *okno)
{
    char tekst[100];
    int pomocnik;
    if(okno->tryb_gracza==0)
    {
        pokaz_info("Wygrałeś!", okno->window);
        ranking *nowy;
        nowy=wczytaj(okno->wysokosc_planszy, okno->szerokosc_planszy, 0);
        pomocnik=policz_sume_kontrolna(nowy);
        if(nowy->suma_kontrolna != pomocnik)    //wykryto zmiane w rankingu
        {
            printf("Policzona suma %d %d\n", nowy->suma_kontrolna, pomocnik);
            pokaz_blad("Wykryto oszustwo w rankingu, ranking zostanie zresetowany!", okno->window);
            free(nowy);
            nowy=utworz();
        }


        sprintf(tekst, "%s\n",gtk_label_get_text(GTK_LABEL(okno->label_nazwa_gracza)));
        dopisz_do_rankingu(tekst,okno->liczba_punktow_gracza, nowy);
        zapisz(okno->wysokosc_planszy, okno->szerokosc_planszy, 0, nowy);

        gtk_widget_destroy(okno->window);
        return;
    }

    if((okno->liczba_punktow_gracza) > (okno->liczba_punktow_przeciwnika))
    {
        sprintf(tekst, "Wygrał %s!", gtk_label_get_text(GTK_LABEL(okno->label_nazwa_gracza)));
        pokaz_info(tekst, okno->window);
        if(okno->tryb_gracza==1)
        {
            ranking *nowy;
            nowy=wczytaj(okno->wysokosc_planszy, okno->szerokosc_planszy, 1);
            pomocnik=policz_sume_kontrolna(nowy);
            if(nowy->suma_kontrolna != pomocnik)    //wykryto zmiane w rankingu
            {
                pokaz_blad("Wykryto oszustwo w rankingu, ranking zostanie zresetowany!", okno->window);
                free(nowy);
                nowy=utworz();
            }

            sprintf(tekst, "%s\n",gtk_label_get_text(GTK_LABEL(okno->label_nazwa_gracza)));
            dopisz_do_rankingu(tekst,okno->liczba_punktow_gracza, nowy);
            zapisz(okno->wysokosc_planszy, okno->szerokosc_planszy, 1, nowy);
        }
        gtk_widget_destroy(okno->window);

        return;
    }
    else
    {
        sprintf(tekst, "Wygrał %s!", gtk_label_get_text(GTK_LABEL(okno->label_nazwa_przeciwnika)));
        pokaz_info(tekst, okno->window);
        if(okno->tryb_gracza==1)
        {
            ranking *nowy;
            nowy=wczytaj(okno->wysokosc_planszy, okno->szerokosc_planszy, 1);
            pomocnik=policz_sume_kontrolna(nowy);
            if(nowy->suma_kontrolna != pomocnik)    //wykryto zmiane w rankingu
            {
                pokaz_blad("Wykryto oszustwo w rankingu, ranking zostanie zresetowany!", okno->window);
                free(nowy);
                nowy=utworz();
            }

            sprintf(tekst, "%s\n",gtk_label_get_text(GTK_LABEL(okno->label_nazwa_przeciwnika)));
            dopisz_do_rankingu(tekst,okno->liczba_punktow_przeciwnika, nowy);
            zapisz(okno->wysokosc_planszy, okno->szerokosc_planszy, 1, nowy);
        }

        gtk_widget_destroy(okno->window);

        return;
    }

}
