#include <stdio.h>
#include <sys/stat.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include "fifo.h"

struct pipes {
    FILE *fifo_in, *fifo_out;
    int isA;
} ;

int fileno(FILE *file);
void pokazBlad(char *komunikat);
static FILE *openOutPipe(char *name);
static FILE *openInPipe(char *name);

void closePipes(PipesPtr pipes)
{
  fclose(pipes->fifo_in);
  fclose(pipes->fifo_out);
  free(pipes);
}

PipesPtr initPipes(char znak)
{
    if (znak!= 'A' && znak!= 'B')
    {
        fprintf(stderr,"\nThis program should be called with one argument: A or B\n\n");
	mkfifo("AtoB",0664);
	mkfifo("BtoA",0664);
	fprintf(stderr,"Fifo queues AtoB and BtoA created\n");
        return NULL;
    }
    PipesPtr pipes=(PipesPtr)malloc(sizeof(struct pipes));
    if (pipes == NULL) {
        fprintf(stderr,"Memory allocation error\n");
    } else {
        pipes->isA=(znak == 'A');
        pipes->fifo_out = openOutPipe(pipes->isA ? "AtoB" : "BtoA");
        if(pipes->fifo_out==NULL)
        {
            free(pipes);
            return NULL;
        }

        pipes->fifo_in = openInPipe(pipes->isA ? "BtoA" : "AtoB");
    }
    if(pipes->fifo_in==NULL)
    {
        closePipes(pipes);
        return NULL;
    }

    return pipes;
}

static FILE *openOutPipe(char *name) {
    FILE *pipe = fopen(name, "w+");
    if (pipe == NULL)
    {
        printf("Error in creating output pipe");
        if(strcmp(name, "BtoA"))
        {
                pokaz_blad("Nie mo�na utworzy� serwera!", widget_okno_glowne);
                return  NULL;
        }
        else
        {
            pokaz_blad("Nie mo�na utworzy� klienta!", widget_okno_glowne);
            return  NULL;
        }
    }
    return pipe;
}

static FILE *openInPipe(char *name){
    FILE *pipe = fopen(name, "r+");
    if (pipe == NULL)
    {
        if(strcmp(name, "BtoA"))
            {
                pokaz_blad("Najpierw nale�y uruchomi� gr� w trybie serwera!", widget_okno_glowne);
                return  NULL;
            }
    }
    int flags, fd;
    fd = fileno(pipe);
    flags = fcntl(fd, F_GETFL);
    fcntl(fd, F_SETFL, flags | O_NONBLOCK);
    return pipe;
}

void sendStringToPipe(PipesPtr pipes, const char *data)
{
    int result = fprintf(pipes->fifo_out,"%s",data);
    fflush(pipes->fifo_out);
    if (result == 0)
        printf("Failed to send data");
}

bool getStringFromPipe(PipesPtr pipes, char *buffer, size_t size)
{
    char *result = fgets(buffer,size,pipes->fifo_in);
    fflush(pipes->fifo_in);
    //if (result == NULL) pokazBlad("Failed to read data");
    return result != NULL;
}
